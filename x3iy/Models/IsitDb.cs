using System.Data.Entity;

namespace x3iy.Models
{
    public class IsitDb : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Link> Links { get; set;  }
        public IsitDb() : base("ISIT01"){
    
        }
    }
}