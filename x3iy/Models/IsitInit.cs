using System;
using System.Collections.Generic;

namespace x3iy.Models
{
    public class IsitInit : System.Data.Entity.DropCreateDatabaseIfModelChanges<IsitDb>
    {
        protected override void Seed(IsitDb context)
        {
            var phone = new List<Phone>
            {
                new Phone {Id= 1,Name="Ivanov",PNumber = "923183123912" },
                new Phone {Id= 2,Name="Ivanov",PNumber = "923183123912" },
                new Phone {Id= 3,Name="Ivanov",PNumber = "923183123912" },
                new Phone {Id= 4,Name="Ivanov",PNumber = "923183123912" },
            };
                
            var students = new List<Student>
            {
                new Student {},
                new Student {Id = 2, Bday = DateTime.Now, Name = "Vasya", Surname = "Ivanov"},
                new Student {Id = 3, Bday = DateTime.Now, Name = "Vasya", Surname = "Ivanov"},
                new Student {Id = 4, Bday = DateTime.Now, Name = "Vasya", Surname = "Ivanov"}
            };
    
            var link = new List<Link>
            {
                new Link {Id = 1, Name = "BSTU", Url = "https://bstu.by"}
            };
                
            phone.ForEach(c => context.Phones.Add(c));
            students.ForEach(c => context.Students.Add(c));
            link.ForEach(c => context.Links.Add(c));
            context.SaveChanges();
        }
    }
}