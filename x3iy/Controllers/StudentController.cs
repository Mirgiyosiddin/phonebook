using System;
using System.Linq;
using System.Web.Mvc;
using x3iy.Models;

namespace x3iy.Controllers
{
    public class StudentController : Controller
    {
        IsitDb db = new IsitDb();

        // GET
        public ActionResult Index()
        {
            Student[] s = db.Students.ToArray<Student>();
            return View(s);
        }
        
        public ActionResult Details(int? id)
        {
            return View();
        }
        
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(Student student)
        {
            try
            {
                db.Students.Add(new Student {Name = student.Name, Surname = student.Surname, Bday = student.Bday});
                db.SaveChanges();
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }
        
        public ActionResult Edit(int? ID)
        {
            Student s = null;
            try
            {
                s = db.Students.Find(ID);
                if (s != null)
                {
                    return View(s);
                }
                // int k = 1;
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public ActionResult ConfirmEdit(Student s)
        {
            Student s1 = null;
            try
            {
                s1 = db.Students.Find(s.Id);
                if (s1 != null)
                {
                    if (TryUpdateModel(s1, "", new string[] {"Name", "Surname", "BDay"}))
                    {
                        db.SaveChanges();
                    }
                }
        
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        public ActionResult Delete(int? id)
        {
            ViewBag.id = id;
            return View();
        }
        
        [HttpPost]
        public ActionResult Delete(int? id, FormCollection collection)
        {
            try
            {
                Student s = db.Students.Find(id);
                if (s != null) db.Students.Remove(s);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        
        }
        
        public ActionResult Find()
        {
            return View();
        }
        
        public ActionResult ConfirmFind(string Name, DateTime? Bday)
        {
            Student[] c = { };
            if (Bday == null)
            {
                c = db.Students.Where(n => n.Name.Contains(Name)).ToArray();
            }
            else
            {
                c = db.Students.Where(n => n.Bday==Bday).ToArray();
            }
            return View (c);
        }
    }
}