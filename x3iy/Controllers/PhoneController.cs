using System.Linq;
using System.Web.Mvc;
using x3iy.Models;

namespace x3iy.Controllers
{
    public class PhoneController : Controller
    {
        private IsitDb db = new IsitDb();
        // GET
        public ActionResult Index()
        {
            Phone[] s = db.Phones.ToArray<Phone>();
            return View(s);
        }
        
        
        public ActionResult Details(int? id)
        {
            return View ();
        }

        public ActionResult Create()
        {
            return View ();
        } 
        
        [HttpPost]
        public ActionResult Create(Phone student)
        {
            try
            {
                db.Phones.Add(new Phone { Name = student.Name, PNumber = student.PNumber });
                db.SaveChanges();
            }
            catch
            {
                return View ();
            }
            return RedirectToAction("Index");
        }
        
        public ActionResult Edit(int? ID)
        {
            Phone s = null;
            try
            {
                s = db.Phones.Find(ID);
                if (s != null)
                {
                    return View(s);
                }
                // int k = 1;
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public ActionResult ConfirmEdit(Phone s)
        {
            Phone s1 = null;
            try
            {
                s1 = db.Phones.Find(s.Id);
                if (s1 != null)
                {
                    if (TryUpdateModel(s1, "", new string[] {"Name", "PNumber"}))
                    {
                        db.SaveChanges();
                    }
                }
        
                return RedirectToAction("Index");
            }
            catch
            {
                return View ();
            }
        }
        
        public ActionResult Delete(int? id)
        {
            ViewBag.id = id;
            return View();
        }
        
        [HttpPost]
        public ActionResult Delete(int? id, FormCollection collection)
        {
            try
            {
                Phone s = db.Phones.Find(id);
                if (s != null) db.Phones.Remove(s);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
            
        }
        
        public ActionResult Find()
        {
            return View ();
        }
        
        public ActionResult ConfirmFind(string name, string PNumber)
        {
            Phone[] c = { };
            if (string.IsNullOrEmpty(PNumber))
            {
                c = db.Phones.Where(n => n.Name.Contains(name)).ToArray();
            }
            else
            {
                c = db.Phones.Where(n => n.PNumber.Equals(PNumber)).ToArray();
            }
            return View (c);
        }
    }
}