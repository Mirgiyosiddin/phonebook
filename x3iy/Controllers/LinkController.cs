using System.Linq;
using System.Web.Mvc;
using x3iy.Models;

namespace x3iy.Controllers
{
    public class LinkController : Controller
    {
        IsitDb db = new IsitDb();

        // GET
        public ActionResult Index()
        {
            Link[] s = db.Links.ToArray<Link>();
            return View(s);
        }
        
        public ActionResult Details(int? id)
        {
            return View();
        }
        
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(Link website)
        {
            try
            {
                db.Links.Add(new Link {Name = website.Name, Url = website.Url});
                db.SaveChanges();
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }
        
        public ActionResult Edit(int? ID)
        {
            Link s = null;
            try
            {
                s = db.Links.Find(ID);
                if (s != null)
                {
                    return View(s);
                }
                // int k = 1;
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public ActionResult ConfirmEdit(Link s)
        {
            Link s1 = null;
            try
            {
                s1 = db.Links.Find(s.Id);
                if (s1 != null)
                {
                    if (TryUpdateModel(s1, "", new string[] {"Name", "Url"}))
                    {
                        db.SaveChanges();
                    }
                }
        
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        public ActionResult Delete(int? id)
        {
            ViewBag.id = id;
            return View();
        }
        
        [HttpPost]
        public ActionResult Delete(int? id, FormCollection collection)
        {
            try
            {
                Link s = db.Links.Find(id);
                if (s != null) db.Links.Remove(s);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        
        }
        
        public ActionResult Find()
        {
            return View();
        }
        
        public ActionResult ConfirmFind(string Name, string Url)
        {
            Link[] c = { };
            if (string.IsNullOrEmpty(Url))
            {
                c = db.Links.Where(n => n.Name.Contains(Name)).ToArray();
            }
            else
            {
                c = db.Links.Where(n => n.Url.Equals(Url)).ToArray();
            }
            return View (c);
        }
    }
}